package com.example.demo;

import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class TestFlux {
	
	public static void main(String[] args) {
		Flux<String> ids = Flux.just("1","2","3","4"); 

		Flux<String> combinations =
				ids.flatMap(id -> { 
					System.out.println("id: " + id);
					Mono<String> nameTask = Mono.just(id); 
//					Mono<Integer> statTask = ifhrStat(id); 

					return nameTask; //.zipWith(statTask, 
							//(name, stat) -> "Name " + name + " has stats " + stat);
				});
		
		Mono<List<String>> result = combinations.collectList();
		List<String> results = result.block();
		System.out.println("results: " + results);
	}

}
