package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.PersonService;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/person")
public class PersonController {
	
	private PersonService personService;
	
	public PersonController(PersonService personService) {
		this.personService=personService;
	}
	
	@GetMapping
	private Flux<User> getAllPersons() {
	    return personService.getAll();
	}

}
