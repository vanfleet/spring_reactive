package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.example.demo.Employee;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EmployeeService {
	private Map<Long,Employee> employeeMap;
	
	@PostConstruct
	public void init() {
		employeeMap=new HashMap<>();
		employeeMap.put(1L, new Employee(1L,"David",45));
		employeeMap.put(2L, new Employee(2L,"Sam",21));
		employeeMap.put(3L, new Employee(3L,"Fred",56));
		employeeMap.put(4L, new Employee(4L,"Max",25));

	}
	
	public Mono<Employee> getEmployee(Long id) {
		return Mono.just(employeeMap.get(id));
	}
	
	public Flux<Employee> getEmployees() {
		return Flux.fromStream(employeeMap.values().stream());
	}

}
