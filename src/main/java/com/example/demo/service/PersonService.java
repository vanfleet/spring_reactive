package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.model.db.Person;
import com.example.demo.model.repository.ReactivePersonRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PersonService {
	
	private ReactivePersonRepository reactivePersonRepository;
	private ModelMapper modelMapper = new ModelMapper();
	
	public PersonService(ReactivePersonRepository reactivePersonRepository) {
		this.reactivePersonRepository = reactivePersonRepository;
	}
	
	@PostConstruct
	public void init() {
		reactivePersonRepository.deleteAll().subscribe();
		
		System.out.println("**** INIT");
		List<Person> personList = new ArrayList<>();
		personList.add(new Person("Max","Crack",27));
		personList.add(new Person("John","Doe",19));
		personList.add(new Person("Fred","Finstone",40));
	
		reactivePersonRepository.saveAll(personList).subscribe();
		//reactivePersonRepository.count().onN
		//System.out.println("Count: " + reactivePersonRepository.count());
	}
	
	public Mono<Person> getPerson(String id) {
		return reactivePersonRepository.findById(id);
	}
	
	public Flux<User> getAll() {
		//Flux<Person> flux = reactivePersonRepository.findAll();
		//Flux<Person> flux = Flux.empty();

		return reactivePersonRepository.findAll().flatMap(person -> {
			return Mono.just(modelMapper.map(person, User.class));
		}).switchIfEmpty(alternates());
		
//		flux.toStream().forEach(person -> {
//			System.out.println("Person: " + person.getFirstname() + " " + person.getId());
//		});
//		flux.doOnEach(ddd -> {System.out.println("ddd: " + ddd);});
//		flux.subscribe();
//		flux.flatMap(p -> {return p;})
//		return flux;
		
	}
	
	private Flux<Person> alternates() {
		return Flux.empty();
	}

}
